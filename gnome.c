/********************************************************************************
*										*
* gnome.c (part of gNiall)							*
* Copyright 1999 Gary Benson <rat@spunge.org>					*
*										*
* This program is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* This program is distributed in the hope that it will be useful,		*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with this program; if not, write to the Free Software			*
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.			*
*										*
********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <gtk/gtk.h>
#include <gnome.h>

#include "niall.h"


/********************************************************************************
* Definitions and globals							*
********************************************************************************/

#define BUFFER_SIZE	256

GtkWidget *windowMain;		/* The Main Window.				*/
GtkWidget *textArea;		/* Where stuff is printed.			*/
GtkWidget *textEntry;		/* Where sentences are entered.			*/
GtkWidget *statusBar;		/* The statusbar at the bottom.			*/

GtkWidget *dialogCorrect=NULL;	/* The correction dialog			*/
GtkWidget *entryIncorrect;
GtkWidget *entryCorrected;

GtkWidget *fileOpen=NULL;	/* File selectors				*/
GtkWidget *fileSave=NULL;

GdkColor _blue  = {0,0x0000,0x0000,0x7777}, *promptColour  = &_blue;
GdkFont *fixedFont;

gboolean wordsSaved=TRUE;
char *fileName=NULL;


/********************************************************************************
* Callbacks from the Correction Dialog						*
********************************************************************************/

static gint correct_destroy(GnomeDialog *d, gpointer data)
{
	dialogCorrect=NULL;
        return(TRUE);
}

static gint correct_clicked(GnomeDialog *d,gint button, gpointer data)
{
	if(GTK_WIDGET_VISIBLE(dialogCorrect)) gtk_widget_hide(GTK_WIDGET(dialogCorrect));

	/* 'Cancel'
	*/
	if(button!=0) return(TRUE);

	/* 'Ok'
	*/
	Niall_CorrectSpelling( gtk_entry_get_text(GTK_ENTRY(entryIncorrect)),
			       gtk_entry_get_text(GTK_ENTRY(entryCorrected)));

	wordsSaved=FALSE;
        return(TRUE);
}


/********************************************************************************
* Callbacks from the File Requesters						*
********************************************************************************/

static gint fileXXXX_destroy(GtkWidget *w, GtkFileSelection *fs)
{
	if( fs==GTK_FILE_SELECTION(fileOpen) ) fileOpen=NULL;
        return(TRUE);
}
static gint fileXXXX_cancel(GtkWidget *w, GtkFileSelection *fs)
{
	if(GTK_WIDGET_VISIBLE(fs)) gtk_widget_hide(GTK_WIDGET(fs));
	return(TRUE);
}
static gint fileOpen_ok(void)
{
	char *fileName_t;

	if(GTK_WIDGET_VISIBLE(fileOpen)) gtk_widget_hide(GTK_WIDGET(fileOpen));

	fileName_t = gtk_file_selection_get_filename(GTK_FILE_SELECTION(fileOpen));

	if(fileName) free(fileName);
	fileName=calloc(strlen(fileName_t)+1,sizeof(char));
	strcpy(fileName,fileName_t);

	Niall_LoadDictionary(fileName);
	wordsSaved=TRUE;

	return(TRUE);
}

static void SaveFile(void);

static gint fileSave_ok(void)
{
	char *fileName_t;

	if(GTK_WIDGET_VISIBLE(fileSave)) gtk_widget_hide(GTK_WIDGET(fileSave));

	fileName_t = gtk_file_selection_get_filename(GTK_FILE_SELECTION(fileSave));
	if(fileName_t == NULL) return(TRUE);

	if(fileName) free(fileName);
	fileName=calloc(strlen(fileName_t)+1,sizeof(char));
	strcpy(fileName,fileName_t);

	SaveFile();

	return(TRUE);
}


/********************************************************************************
* Helper functions								*
********************************************************************************/

/* Print a line of text to the textArea
*/
static void printToTextArea(char *Buffer,GdkColor *Fore,GdkFont *Font)
{
	gtk_text_insert(GTK_TEXT(textArea),Font,Fore,NULL,Buffer,strlen(Buffer));
}

/* Save the dictionary under a different name
*/
static void SaveFileAs(void)
{
	if(fileSave == NULL)
	{
		fileSave = gtk_file_selection_new(_("Save File As..."));
		gtk_file_selection_hide_fileop_buttons(GTK_FILE_SELECTION(fileSave));
		gtk_signal_connect(GTK_OBJECT(fileSave), "destroy",
			(GtkSignalFunc)fileXXXX_destroy, fileSave);
		gtk_signal_connect(GTK_OBJECT(
			GTK_FILE_SELECTION(fileSave)->ok_button), 
			"clicked", (GtkSignalFunc)fileSave_ok, NULL);
		gtk_signal_connect(GTK_OBJECT(
			GTK_FILE_SELECTION(fileSave)->cancel_button),
			"clicked", (GtkSignalFunc)fileXXXX_cancel, fileSave);
	}
	if(GTK_WIDGET_VISIBLE(fileSave)==FALSE)
	{
		gtk_widget_show(fileSave);
	}
}

/* Save the dictionary
*/
static void SaveFile(void)
{
	if(fileName==NULL)
	{
		SaveFileAs();
		return;
	}
	Niall_SaveDictionary(fileName);
	wordsSaved=TRUE;
}

/* The 'Do you want to save your dictionary?' box.
*/
static gboolean OkayToContinue(void)
{
	GtkWidget *msgBox;
	int ret;

	if(wordsSaved==TRUE) return(TRUE);

	msgBox = gnome_message_box_new(_("The dictionary has been modified.\nDo you wish to save it?"),
		 GNOME_MESSAGE_BOX_QUESTION,
       		 GNOME_STOCK_BUTTON_YES, GNOME_STOCK_BUTTON_NO,
		 GNOME_STOCK_BUTTON_CANCEL, NULL);

	ret = gnome_dialog_run_and_close(GNOME_DIALOG(msgBox));

	switch(ret)
	{
		case 0:
			SaveFile();
			return(wordsSaved);
			break;
		case 1:
			return(TRUE);
			break;
		case 2:
			return(FALSE);
	}
	fprintf(stderr,"Error: Control should never reach here - OkayToContinue()!\n");
	return(TRUE);
}


/********************************************************************************
* Callbacks from the Main Window						*
********************************************************************************/

/* Clear the dictionary.
*/
static void file_new_cb(void)
{
	if(OkayToContinue()==FALSE) return;

	Niall_NewDictionary();
	if(fileName==NULL) free(fileName);
	fileName=NULL;
	wordsSaved=TRUE;
}

/* Read in a dictionary file.
*/
static void file_open_cb(void)
{
	if(OkayToContinue()==FALSE) return;

	if(fileOpen == NULL)
	{
		fileOpen = gtk_file_selection_new(_("Open File..."));
		gtk_file_selection_hide_fileop_buttons(GTK_FILE_SELECTION(fileOpen));
		gtk_signal_connect(GTK_OBJECT(fileOpen), "destroy",
			(GtkSignalFunc)fileXXXX_destroy, fileOpen);
		gtk_signal_connect(GTK_OBJECT(
			GTK_FILE_SELECTION(fileOpen)->ok_button), 
			"clicked", (GtkSignalFunc)fileOpen_ok, NULL);
		gtk_signal_connect(GTK_OBJECT(
			GTK_FILE_SELECTION(fileOpen)->cancel_button),
			"clicked", (GtkSignalFunc)fileXXXX_cancel, fileOpen);
	}
	if(GTK_WIDGET_VISIBLE(fileOpen)==FALSE)
	{
		gtk_widget_show(fileOpen);
	}
}

/* Save the file under the previous filename
*/
static void file_save_cb(void)
{
	SaveFile();
}

/* Save the file under a different name
*/
static void file_save_as_cb(void)
{
	SaveFileAs();
}

/* Dump the dictionary to the screen
*/
static void file_list_cb(void)
{
	Niall_ListDictionary();
}

/* Correct a spelling
*/
static void file_correct_cb(void)
{
	GtkWidget *label;

	if(dialogCorrect==NULL)
	{
		dialogCorrect = gnome_dialog_new("Corrections",
			GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
		gtk_signal_connect(GTK_OBJECT(dialogCorrect), "destroy",
			(GtkSignalFunc)correct_destroy, NULL);
		gtk_signal_connect(GTK_OBJECT(dialogCorrect), "clicked",
			(GtkSignalFunc)correct_clicked, NULL);

		label = gtk_label_new("Incorrect word:");
		gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialogCorrect)->vbox), label, FALSE, TRUE, 0);
		gtk_widget_show(label);

		entryIncorrect = gtk_entry_new();
		gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialogCorrect)->vbox), entryIncorrect, FALSE, TRUE, 0);
		gtk_widget_show(entryIncorrect);

		label = gtk_label_new("Corrected spelling:");
		gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialogCorrect)->vbox), label, FALSE, TRUE, 0);
		gtk_widget_show(label);

		entryCorrected = gtk_entry_new();
		gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialogCorrect)->vbox), entryCorrected, FALSE, TRUE, 0);
		gtk_widget_show(entryCorrected);
	}
	if(GTK_WIDGET_VISIBLE(dialogCorrect)==FALSE)
	{
		gtk_widget_show(dialogCorrect);
		gtk_entry_set_text(GTK_ENTRY(entryIncorrect),"");
		gtk_entry_set_text(GTK_ENTRY(entryCorrected),"");
		gtk_widget_grab_focus(entryIncorrect);
	}
}

/* Clean up and exit.
*/
static void file_exit_cb(void)
{
	if(OkayToContinue()==FALSE) return;

	Niall_Free();
	exit(EXIT_SUCCESS);
}

/* The 'About this program' box
*/
static void help_about_cb(void)
{
	const gchar *authors[] =
	{
		N_("Gary Benson"),
		N_("http://rat.spunge.org"),
		"",
		N_("gNiall is a complete rewrite of NIALL,"),
		N_("  by Matthew Peck."),
		N_("NIALL itself was a complete rewrite of DANI,"),
		N_("  by an unknown author."),
		NULL
	};
	GtkWidget *windowAbout;

	windowAbout = gnome_about_new (PACKAGE, VERSION,
		_("(C) 1999 Gary Benson/Dirty Rat Software"),
		authors,
		_("gNiall (`genial') attempts to learn a language from sentences that you type in. "	\
		"Occasionally it will say something profound..."),
		_("http://rat.spunge.org"));

	gtk_widget_show(windowAbout);
}

/* Show the user guide
*/
static void help_manual_cb(void)
{
	GtkWidget *msgBox;

	msgBox = gnome_message_box_new(_("Coming soon in gNiall v1.0"),
		 GNOME_MESSAGE_BOX_WARNING,
       		 GNOME_STOCK_BUTTON_OK,NULL);
	gnome_dialog_run_and_close(GNOME_DIALOG(msgBox));
}

/* Learn from a line of text, and then say something back.
*/
static void sf_processLine(void)
{
	char *Buffer;
	size_t buffer_size;
	void Niall_Error( char *fmt, ... );
	
	gnome_appbar_set_status(GNOME_APPBAR(statusBar), "");

	/* Allocate a buffer big enough for the text, but not less
	** than BUFFER_SIZE so there is enough room for Niall to reply
	*/
	buffer_size = strlen(gtk_entry_get_text(GTK_ENTRY(textEntry))) + 1;
	buffer_size = buffer_size < BUFFER_SIZE ? BUFFER_SIZE : buffer_size;
	if(!( Buffer = malloc( buffer_size ) )) Niall_Error( "Out of memory" );

	strncpy( Buffer, gtk_entry_get_text(GTK_ENTRY(textEntry)), buffer_size );
	gtk_entry_set_text(GTK_ENTRY(textEntry),"");

	printToTextArea("User: ",promptColour,NULL);
	printToTextArea(Buffer,NULL,NULL);
	printToTextArea("\n",NULL,NULL);

	Niall_Learn(Buffer);
	Niall_Reply(Buffer,buffer_size);

	printToTextArea("Niall: ",promptColour,NULL);
	printToTextArea(Buffer,NULL,NULL);
	printToTextArea("\n",NULL,NULL);

	free( Buffer );
	wordsSaved=FALSE;
}


/********************************************************************************
* Initialise the user interface							*
********************************************************************************/

GnomeUIInfo file_menu[]=
{
	GNOMEUIINFO_MENU_NEW_ITEM(N_("_New"), N_("Create a new document"), file_new_cb, NULL),
	GNOMEUIINFO_MENU_OPEN_ITEM(file_open_cb, (gpointer) NULL),
	GNOMEUIINFO_MENU_SAVE_ITEM(file_save_cb, (gpointer) NULL),
	GNOMEUIINFO_MENU_SAVE_AS_ITEM(file_save_as_cb, (gpointer) NULL),

	GNOMEUIINFO_SEPARATOR, 
	{
	  GNOME_APP_UI_ITEM, N_("_View Dictionary"), N_("View the word associations that Niall has built"),
	  file_list_cb, NULL, NULL,
	  GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BOOK_OPEN
	},
	{
	  GNOME_APP_UI_ITEM, N_("_Correct Spelling..."), N_("Correct the spelling of a word"),
	  file_correct_cb, NULL, NULL,
	  GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF
	},
	GNOMEUIINFO_SEPARATOR, 

	GNOMEUIINFO_MENU_EXIT_ITEM(file_exit_cb, NULL),
	GNOMEUIINFO_END
};
GnomeUIInfo help_menu[]=
{
	GNOMEUIINFO_MENU_ABOUT_ITEM(help_about_cb, NULL),
	{
	  GNOME_APP_UI_ITEM, N_("User _Guide..."), N_("View the user guide"),
	  help_manual_cb, NULL, NULL,
	  GNOME_APP_PIXMAP_NONE, NULL
	},
	GNOMEUIINFO_END	
};
GnomeUIInfo menu_bar[]=
{
	GNOMEUIINFO_MENU_FILE_TREE(file_menu),          
	GNOMEUIINFO_MENU_HELP_TREE(help_menu),
	GNOMEUIINFO_END
};
GnomeUIInfo tool_bar[]=
{
	/* NB. The second item in each is the button's text - I set them all
	** to NULL as I like them better that way.
	*/
	{ GNOME_APP_UI_ITEM, NULL, N_("Create a new document"), file_new_cb,
	  NULL, NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_NEW },
	{ GNOME_APP_UI_ITEM, NULL, N_("Open a file"), file_open_cb,
	  NULL, NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_OPEN },
	{ GNOME_APP_UI_ITEM, NULL, N_("Save the current file"), file_save_cb,
	  NULL, NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SAVE },

	GNOMEUIINFO_SEPARATOR,

	{ GNOME_APP_UI_ITEM, NULL, N_("View the word associations"), file_list_cb,
	  NULL, NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_BOOK_OPEN },
	{ GNOME_APP_UI_ITEM, NULL, N_("Correct the spelling of a word"), file_correct_cb,
	  NULL, NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_PREFERENCES },

	GNOMEUIINFO_SEPARATOR,

	{ GNOME_APP_UI_ITEM, NULL, N_("Exit the program"), file_exit_cb,
	  NULL, NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_QUIT },

	GNOMEUIINFO_END
};

static void BuildMainWindow(void)
{
	GtkWidget *win,*vbox,*hbox,*status;
	GtkWidget *text,*vscrollbar;
	GtkWidget *label,*entry;

	fixedFont = gdk_font_load("6x13");

	win = gnome_app_new(PACKAGE,PACKAGE VERSION);
	gtk_widget_set_usize(GTK_WIDGET(win), 512, 384);
	gtk_signal_connect(GTK_OBJECT(win), "delete_event", GTK_SIGNAL_FUNC(file_exit_cb), win);
	gtk_window_set_title(GTK_WINDOW(win), "gNiall");
	gtk_container_border_width(GTK_CONTAINER(win), 0);
	windowMain = win;

	status = gnome_appbar_new(FALSE, TRUE, GNOME_PREFERENCES_USER);
	gnome_app_set_statusbar(GNOME_APP(win),GTK_WIDGET(status));
	gnome_appbar_set_status(GNOME_APPBAR(status), "Get Typing...");
	statusBar = status;

	gnome_app_create_menus(GNOME_APP(win), menu_bar);
	gnome_app_install_menu_hints(GNOME_APP(win), menu_bar);

	gnome_app_create_toolbar(GNOME_APP(win),tool_bar);

	vbox = gtk_vbox_new(FALSE, 0);
	gnome_app_set_contents(GNOME_APP(win), vbox);
	gtk_container_border_width(GTK_CONTAINER(vbox), 0);
	gtk_widget_show(vbox);

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
	gtk_container_border_width(GTK_CONTAINER(hbox), 0);
	gtk_widget_show(hbox);

	text = gtk_text_new(NULL,NULL);
	gtk_box_pack_start(GTK_BOX(hbox), text, TRUE, TRUE, 0);
	gtk_text_set_editable(GTK_TEXT(text),FALSE);
	gtk_text_set_word_wrap(GTK_TEXT(text), TRUE);
	gtk_widget_show(text);
	textArea = text;

	vscrollbar = gtk_vscrollbar_new(GTK_TEXT(text)->vadj);
	gtk_box_pack_start(GTK_BOX(hbox), vscrollbar, FALSE, TRUE, 0);
	gtk_widget_show(vscrollbar);

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);
	gtk_container_border_width(GTK_CONTAINER(hbox), 0);
	gtk_widget_show(hbox);

	label = gtk_label_new(" User:  ");
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);
	gtk_widget_show(label);

	entry = gtk_entry_new_with_max_length(BUFFER_SIZE);
	gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);
	gtk_signal_connect_object(GTK_OBJECT(entry), "activate", GTK_SIGNAL_FUNC(sf_processLine), NULL);
	gtk_widget_show(entry);
	textEntry = entry;
}

static void BuildGUI(void)
{
	BuildMainWindow();
}

int main(int argc, char *argv[])
{
	Niall_Init();
	gnome_init(PACKAGE,VERSION,argc,argv);

	BuildGUI();
	gtk_widget_show(windowMain);
	gtk_widget_grab_focus(textEntry);
	gtk_main();
	fprintf(stderr,"Error: Control should never reach here - main()!\n");
	exit(EXIT_FAILURE);
}


/********************************************************************************
* Functions required by niall.c							*
********************************************************************************/

void Niall_Print( char *fmt, ... )
{
	char Buffer[BUFSIZ];
	va_list ap;

	va_start( ap, fmt );
	vsprintf( Buffer, fmt, ap );
	va_end( ap );

	printToTextArea(Buffer,NULL,fixedFont);
}

void Niall_Warning( char *fmt, ... )
{
	char Buffer[BUFSIZ];
	GtkWidget *msgBox;
	va_list ap;

	va_start( ap, fmt );
	vsprintf( Buffer, fmt, ap );
	va_end( ap );

	msgBox = gnome_message_box_new(_(Buffer),
		 GNOME_MESSAGE_BOX_WARNING,
       		 GNOME_STOCK_BUTTON_OK,NULL);
	gnome_dialog_run_and_close(GNOME_DIALOG(msgBox));
}

void Niall_Error( char *fmt, ... )
{
	char Buffer[BUFSIZ];
	GtkWidget *msgBox;
	va_list ap;

	va_start( ap, fmt );
	vsprintf( Buffer, fmt, ap );
	va_end( ap );

	msgBox = gnome_message_box_new(_(Buffer),
		 GNOME_MESSAGE_BOX_ERROR,
       		 GNOME_STOCK_BUTTON_OK,NULL);
	gnome_dialog_run_and_close(GNOME_DIALOG(msgBox));

	exit(EXIT_FAILURE);
}

/*******************************************************************************/

