#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "niall.h"

#define BUFFER_SIZE	256

#define FALSE		0
#define TRUE		1

#define ACT_ERROR	0
#define ACT_NONE	1
#define ACT_EXIT	2
#define ACT_LIST	3
#define ACT_NEW		4
#define ACT_LOAD	5
#define ACT_SAVE	6
#define ACT_CORRECT	7
#define ACT_HELP	8

void Niall_Print( char *fmt, ... )
{
	va_list ap;
	va_start( ap, fmt );
	vfprintf( stderr, fmt, ap );
	va_end( ap );
}

void Niall_Warning( char *fmt, ... )
{
	va_list ap;

	fprintf( stderr, "%s: ", "Warning" );
	va_start( ap, fmt );
	vfprintf( stderr, fmt, ap );
	va_end( ap );
	fprintf( stderr,"\n");
}

void Niall_Error( char *fmt, ... )
{
	va_list ap;

	fprintf( stderr, "%s: ", "Error" );
	va_start( ap, fmt );
	vfprintf( stderr, fmt, ap );
	va_end( ap );
	fprintf( stderr,"\n");
	exit(EXIT_FAILURE);
}

int Action(char *Buffer)
{
	int i;

	if(feof(stdin))
	{
		printf("#exit\n");
		return(ACT_EXIT);
	}
	if(Buffer[0]!='#')	        return(ACT_NONE);
	for(i=0;i<strlen(Buffer);i++)	if(isspace(Buffer[i])) Buffer[i]=0;

	if(!strcmp(&Buffer[1],"exit"))		return(ACT_EXIT);
	if(!strcmp(&Buffer[1],"list"))		return(ACT_LIST);
	if(!strcmp(&Buffer[1],"new"))		return(ACT_NEW);
	if(!strcmp(&Buffer[1],"load"))		return(ACT_LOAD);
	if(!strcmp(&Buffer[1],"save"))		return(ACT_SAVE);
	if(!strcmp(&Buffer[1],"correct")) 	return(ACT_CORRECT);
	if(!strcmp(&Buffer[1],"help"))	 	return(ACT_HELP);

	return(ACT_ERROR);
}

int main(void)
{
	char Buffer[BUFFER_SIZE];
	char Buffer2[BUFFER_SIZE];
	char exit_flag=FALSE;

	Niall_Init();

	do
	{
		printf("User:  ");
		fflush(stdout);
		fgets(Buffer,BUFFER_SIZE,stdin);
		if(strlen(Buffer)<1) return;

		Buffer[strlen(Buffer)-1]=0;
		switch(Action(Buffer))
		{
		    case ACT_NONE:
			Niall_Learn(Buffer);
			Niall_Reply(Buffer,BUFFER_SIZE);
			printf("Niall: %s\n",Buffer);
			break;
		    case ACT_EXIT:
			printf("Niall: Bye...\n");
			exit_flag=TRUE;
			break;
		    case ACT_LIST:
			Niall_ListDictionary();
			break;
		    case ACT_NEW:
			Niall_NewDictionary();
			break;
		    case ACT_LOAD:
			printf(" Filename to LOAD: ");
			fflush(stdout);
			fgets(Buffer,BUFFER_SIZE,stdin);
			if(strlen(Buffer)<1) return;
			Buffer[strlen(Buffer)-1]=0;
			Niall_LoadDictionary(Buffer);
			break;
		    case ACT_SAVE:
			printf(" Filename to SAVE: ");
			fflush(stdout);
			fgets(Buffer,BUFFER_SIZE,stdin);
			if(strlen(Buffer)<1) return;
			Buffer[strlen(Buffer)-1]=0;
			Niall_SaveDictionary(Buffer);
			break;
		    case ACT_CORRECT:
			printf(" Enter incorrect word: ");
			fflush(stdout);
			fgets(Buffer,BUFFER_SIZE,stdin);
			if(strlen(Buffer)<1) return;
			Buffer[strlen(Buffer)-1]=0;
			printf(" Enter correct spelling: ");
			fflush(stdout);
			fgets(Buffer2,BUFFER_SIZE,stdin);
			if(strlen(Buffer2)<1) return;
			Buffer2[strlen(Buffer2)-1]=0;
			Niall_CorrectSpelling(Buffer,Buffer2);
			break;
		    case ACT_HELP:
			printf("\n\t#new     - Clear the dictionary.");
			printf("\n\t#load    - Load a dictionary.");
			printf("\n\t#save    - Save the dictionary.");
			printf("\n\t#list    - View the dictionary.");
			printf("\n\t#correct - Correct a spelling.");
			printf("\n\t#help    - This help.");
			printf("\n\t#exit    - Quit Niall.");
			printf("\n\n");
			break;
		    default:
			printf("  Unknown command - type #help for help\n");
		}
	}
	while(!exit_flag);

	Niall_Free();
}
